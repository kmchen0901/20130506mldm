SubWin_Samplers.Forward_Samplor = function (x.xts, SubWin_Size = 20, Events_Dates = NULL, normailize = TRUE, 
          BasePrice_Gen = NULL) 
{
  if (class(Events_Dates) == "character") 
    Events_Dates <- as.Date(Events_Dates)
  
  XTS_Dates = rownames(as.matrix(x.xts))
  if (is.null(Events_Dates)) {
    SubWins_Dates_Index = 1:length(XTS_Dates)
    SubWins_Dates = XTS_Dates
  }
  else {
    SubWins_Dates_Index = which(XTS_Dates %in% Events_Dates)
    SubWins_Dates = XTS_Dates[SubWins_Dates_Index]
  }
  XTS_Length = length(XTS_Dates)
  if (SubWin_Size > 0) {
    SubWins_Dates_Index = SubWins_Dates_Index[which((XTS_Length - 
                                                       SubWins_Dates_Index) > SubWin_Size)]
    SubWins_Dates = XTS_Dates[SubWins_Dates_Index]
    SubWins_Index = cbind(SubWins_Dates, XTS_Dates[SubWins_Dates_Index + 
                                                     SubWin_Size])
    SubWins_Index = apply(SubWins_Index, 1, function(xx) paste(xx[1], 
                                                               xx[2], sep = "::"))
  }
  else {
    SubWins_Index = paste(SubWins_Dates, "::", sep = "")
  }
  if (normailize) {
    SubWins = lapply(SubWins_Index, function(xx) {
      SubWin_XTS = x.xts[xx, ]
      BasePrice = ifelse(test = is.null(BasePrice_Gen), 
                         yes = as.numeric(SubWin_XTS[1, 1]), no = BasePrice_Gen(SubWin_XTS))
      SubWin_XTS = (SubWin_XTS - BasePrice)/BasePrice
      return(list(BasePrice = BasePrice, ReturnSubWin = SubWin_XTS))
    })
    return(SubWins)
  }
  else {
    SubWins = lapply(SubWins_Index, function(xx) {
      SubWin_XTS = x.xts[xx, ]
      return(list(ReturnSubWin = SubWin_XTS))
    })
    return(SubWins)
  }
}


getSymbols("^TWII",from="2001-01-01")
getSymbols("^SOX",from="2001-01-01")

K_Centers = 5
WinSize = 20
test = SubWin_Samplers.Forward_Samplor(x.xts=Cl(SOX),SubWin_Size=WinSize)
# test_data = do.call(rbind,lapply(test,function(xx) as.numeric(diff(xx$ReturnSubWin)[-1])))
test_data = do.call(rbind,lapply(test,function(xx) as.numeric(xx$ReturnSubWin)))
test_means = kmeans(test_data,K_Centers,iter.max=50)
layout(as.matrix(cbind(2:(K_Centers+1),2:(K_Centers+1),1,1)))
layout.show((K_Centers+1))
par(mar=c(3,3,1,1))
barplot(test_means$size, horiz=TRUE)
for (i in 1:K_Centers){
  par(mar=c(2,2,1,1))
  plot(1:(WinSize+1),test_means$centers[i,],type="l",col=i)
}

par(mar=c(1,1))
chartSeries(SOX)
plot(1:(WinSize+1),test_means$centers[1,],type="l",ylim=c(-0.3,0.3))
for (i in 2:K_Centers){
  points(1:(WinSize+1),test_means$centers[i,],type="l",col=i)
}

